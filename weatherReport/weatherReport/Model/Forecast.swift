//
//  Forecast.swift
//  weatherReport
//
//  Created by primeIT on 22/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation
import UIKit

class Forecast{
    let mCity: String
    let mDate: String
    let mMinTemp: Float
    let mMaxTemp: Float
    let mCurrentTemp: Float
    let mState: ForecastState
    let mIcon: UIImage?
    
    required init!(date: String, currentTemp: Float = 0, minTemp:Float, maxTemp:Float, state:ForecastState){
        guard minTemp <= maxTemp else{
            return nil
        }
        
        let icon = UIImage(named: state.rawValue)
        mCity = UserDefaultUtils.getLocation()
        mDate = date
        mCurrentTemp = currentTemp
        mMinTemp = minTemp
        mMaxTemp = maxTemp
        mState = state
        mIcon = icon
    }
    
    convenience init!(withJson json: [String:Any]){
        guard
            let weather = json["weather"] as? [[String:Any]],
            let main = json["main"] as? [String:Any],
            let stateString = weather[0]["main"] as? String,
            let temp = main["temp"] as? Float,
            let minTemp = main["temp_min"] as? Float,
            let maxTemp = main["temp_max"] as? Float else{
                return nil
        }
        
        let state = ForecastState(rawValue: stateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE d MMM"
        let date = dateFormatter.string(from: Date())
        self.init(date: date, currentTemp: temp, minTemp: minTemp, maxTemp: maxTemp, state: state ?? .None)
    }
}

///Represents the possible weather states. It's important to note that raw values correspond to the image names in the assets
enum ForecastState: String{
    case Cloudy = "Clouds"
    case Rainy = "Rain"
    case Stormy = "storm"
    case Sunny = "Clear"
    case None = "Unknown"
}
