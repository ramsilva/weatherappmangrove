//
//  ForecastProvider.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

///Implemented by any class that wishes to act as a data source for weather and forecasts
protocol ForecastProvider {
    var mDelegate: ForecastProviderDelegate {get}
    var mCurrentWeater: Forecast? {get}
    var mForecasts: [Forecast] {get}
    
    func getCurrentWeater(city: String)
    func getForecast(city: String, forDays days:Int)
}
