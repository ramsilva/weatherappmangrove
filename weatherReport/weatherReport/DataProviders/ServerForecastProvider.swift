//
//  ServerForecastProvider.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

class ServerForecastProvider: ForecastProvider{
    var mDelegate: ForecastProviderDelegate
    var mCurrentWeater: Forecast?
    var mForecasts = [Forecast]()
    
    required init(withDelegate delegate:ForecastProviderDelegate){
        mDelegate = delegate
    }
    
    func getCurrentWeater(city: String) {
        let code = LocationUtils.getCityCode(city)
        let task = URLSession.shared.dataTask(with: URL(string:"https://api.openweathermap.org/data/2.5/weather?id=\(code)&APPID=5b812b2fc84ff854cc4c25764a568e12")!){ data, response, error in
            do{
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else{
                    return
                }
                self.mDelegate.onCurrentWeather(weather: Forecast(withJson: json))
            } catch {
                
            }
        }
        task.resume()
    }
    
    func getForecast(city: String, forDays days: Int) {
        let code = LocationUtils.getCityCode(city)
        let task = URLSession.shared.dataTask(with: URL(string:"https://api.openweathermap.org/data/2.5/forecast?id=\(code)&APPID=5b812b2fc84ff854cc4c25764a568e12")!){ data, response, error in
            do{
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] else{
                    return
                }
                guard let forecasts = ForecastUtil.getForecasts(fromJson: json, forDays: days) else{
                    return
                }
                self.mDelegate.onForecast(forecasts: forecasts)
            } catch {}
        }
        task.resume()
    }
}
