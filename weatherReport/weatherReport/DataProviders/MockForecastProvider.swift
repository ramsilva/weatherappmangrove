//
//  MockForecastProvider.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

class MockForecastProvider: ForecastProvider{
    var mDelegate: ForecastProviderDelegate
    var mCurrentWeater: Forecast?
    var mForecasts = [Forecast]()
    
    required init(withDelegate delegate:ForecastProviderDelegate){
        mDelegate = delegate
        mCurrentWeater = Forecast(date: "Today", currentTemp: 100, minTemp: 98, maxTemp: 102, state: .Cloudy)
        mForecasts.append(Forecast(date: "Tomorrow", minTemp: 2, maxTemp: 7, state: .Cloudy))
        mForecasts.append(Forecast(date: "Sun", minTemp: 100, maxTemp: 105, state: .Rainy))
        mForecasts.append(Forecast(date: "Mon", minTemp: 27, maxTemp: 31, state: .Stormy))
        mForecasts.append(Forecast(date: "Tue", minTemp: 2, maxTemp: 12, state: .Sunny))
        mForecasts.append(Forecast(date: "Wed", minTemp: 98, maxTemp: 100, state: .Sunny))
    }
    
    func getCurrentWeater(city: String) {
        mDelegate.onCurrentWeather(weather: mCurrentWeater!)
    }
    
    func getForecast(city: String, forDays days: Int) {
        mDelegate.onForecast(forecasts: mForecasts)
    }
    
}
