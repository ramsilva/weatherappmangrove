//
//  ForecastProviderDelegate.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

protocol ForecastProviderDelegate {
    func onCurrentWeather(weather: Forecast)
    func onForecast(forecasts: [Forecast])
}
