//
//  SettingsTableViewController.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate{
    //MARK: Properties
    @IBOutlet var mTableView: UITableView!{
        didSet{
            mOriginViewController.mScrollView = mTableView
            mTableView.bounces = false
        }
    }
    @IBOutlet weak var mSelectedLocationLabel: UILabel!{
        didSet{
            mSelectedLocationLabel.text = UserDefaultUtils.getLocation().lowercased().capitalizingFirstLetter()
        }
    }
    @IBOutlet weak var mSelectedUnitLabel: UILabel!{
        didSet{
            mSelectedUnitLabel.text = UserDefaultUtils.getUnits().rawValue
        }
    }
    @IBOutlet weak var mSelectedForecastDaysLabel: UILabel!{
        didSet{
            mSelectedForecastDaysLabel.text = String(UserDefaultUtils.getForecastDays())
        }
    }
    var mOriginViewController: SettingsViewController!
    var mPossibleUnits = [TemperatureUnit]()
    var mPickerAlert: UIAlertController!
    var mSelectedUnit: TemperatureUnit!
    var mSelectedForecastDays: Int!
    
    //MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        mTableView.tableFooterView = UIView()
        
        mPossibleUnits.append(.Celsius)
        mPossibleUnits.append(.Kelvin)
        mPossibleUnits.append(.Fahrenheit)
        
        mSelectedUnit = mPossibleUnits[0]
        mSelectedForecastDays = 1
        
    }
    
    //MARK: TableView delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.row{
        case 0:
            let alert = UIAlertController(title: NSLocalizedString("Location", comment: ""), message: NSLocalizedString("Location message", comment: ""), preferredStyle: .alert)
            alert.addTextField(configurationHandler: nil)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default){[unowned self] _ in
                if !UserDefaultUtils.setLocation(alert.textFields![0].text){
                    let errorAlert = UIAlertController(title: NSLocalizedString("Invalid city", comment: ""), message: String(format: NSLocalizedString("Invalid city message", comment: ""),LocationUtils.getAvailableCities()), preferredStyle: .alert)
                    errorAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(errorAlert, animated: false, completion: nil)
                }else{
                    self.mSelectedLocationLabel.text = alert.textFields![0].text?.lowercased().capitalizingFirstLetter()
                }
            })
            present(alert, animated: true, completion: nil)
            print(0)
        case 1:
            mPickerAlert = UIAlertController(title: "Temperature units", message: nil, preferredStyle: .actionSheet)
            mPickerAlert.addAction(UIAlertAction(title: "Ok", style: .default){[unowned self] (_) in
                UserDefaults.standard.set(self.mSelectedUnit.rawValue, forKey: UserDefaultUtils.UNITS_KEY)
                self.mSelectedUnitLabel.text = self.mSelectedUnit.rawValue
            })
            mPickerAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            let picker = UIPickerView (frame: CGRect.zero)
            picker.tag = 1
            picker.dataSource = self
            picker.delegate = self
            mPickerAlert.view.addSubview(picker)
            mPickerAlert.view.addConstraint(NSLayoutConstraint(item: mPickerAlert.view, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: picker, attribute: .height, multiplier: 1, constant: 100))
            mPickerAlert.view.addConstraint(NSLayoutConstraint(item: mPickerAlert.view, attribute: .centerX, relatedBy: .equal, toItem: picker, attribute: .centerX, multiplier: 1, constant: 0))
            
            present(mPickerAlert, animated: true, completion: nil)
            print(1)
        case 2:
            mPickerAlert = UIAlertController(title: "Number of days", message: nil, preferredStyle: .actionSheet)
            mPickerAlert.addAction(UIAlertAction(title: "Ok", style: .default){[unowned self] (_) in
                UserDefaults.standard.set(self.mSelectedForecastDays, forKey: UserDefaultUtils.FORECAST_DAYS_KEY)
                self.mSelectedForecastDaysLabel.text = String(self.mSelectedForecastDays)
            })
            mPickerAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            let picker = UIPickerView (frame: CGRect.zero)
            picker.tag = 2
            picker.dataSource = self
            picker.delegate = self
            mPickerAlert.view.addSubview(picker)
            mPickerAlert.view.addConstraint(NSLayoutConstraint(item: mPickerAlert.view, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: picker, attribute: .height, multiplier: 1, constant: 100))
            mPickerAlert.view.addConstraint(NSLayoutConstraint(item: mPickerAlert.view, attribute: .centerX, relatedBy: .equal, toItem: picker, attribute: .centerX, multiplier: 1, constant: 0))
            
            present(mPickerAlert, animated: true, completion: nil)
            print(2)
        default:
            break
        }
    }
    
    /*
     The HidingFooterViewDelegate methods have to be called manually has a workaround for tableViews, as the delegate can't be overriden.
     Ideally the functionality should be implemented using an Observer pattern.
     */
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        mOriginViewController.mScrollViewDelegate.scrollViewWillBeginDragging(scrollView)
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        mOriginViewController.mScrollViewDelegate.scrollViewDidEndDragging(scrollView, willDecelerate: decelerate)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        mOriginViewController.mScrollViewDelegate.scrollViewDidScroll(scrollView)
    }
    
    //MARK: Picker delegate and data source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1{
            return mPossibleUnits.count
        }else{
            return 5
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1{
            return mPossibleUnits[row].rawValue
        }else{
            return String(row + 1)
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1{
            mSelectedUnit = mPossibleUnits[row]
        }else{
            mSelectedForecastDays = row + 1
        }
    }
}
