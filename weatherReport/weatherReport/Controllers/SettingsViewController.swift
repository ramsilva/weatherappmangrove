//
//  SettingsViewController.swift
//  weatherReport
//
//  Created by primeIT on 25/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, HidingFooterDelegate {
    //MARK: Properties
    
    @IBOutlet weak var mFooterView: UIView!
    @IBOutlet weak var mFooterBottomConstraint: NSLayoutConstraint!
    
    var mScrollViewDelegate: HidingFooterScrollViewDelegate!
    var mScrollView: UIScrollView!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mScrollViewDelegate = HidingFooterScrollViewDelegate(withDelegate: self)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SettingsEmbedSegue"{
            if let childViewController = segue.destination as? SettingsTableViewController{
                childViewController.mOriginViewController = self
            }
        }
    }
    
    //MARK: HidingFooterDelegate
    func hideFooter() {
        mFooterBottomConstraint.constant = -70
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func showFooter() {
        mFooterBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

}
