//
//  ExtraInfoViewController.swift
//  weatherReport
//
//  Created by primeIT on 24/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

class ExtraInfoViewController: UIViewController, HidingFooterDelegate {

    //MARK: Properties
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var mFooterView: UIView!
    @IBOutlet weak var mFooterBottomConstraint: NSLayoutConstraint!
    
    var mIsDragging = false
    var mScrollViewDelegate: HidingFooterScrollViewDelegate!
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mScrollViewDelegate = HidingFooterScrollViewDelegate(withDelegate: self)
        //mScrollView.alwaysBounceVertical = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mScrollView.delegate = mScrollViewDelegate
    }
    
    //MARK: HidingFooterDelegate
    func hideFooter(){
        mFooterBottomConstraint.constant = -70
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    func showFooter(){
        mFooterBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
