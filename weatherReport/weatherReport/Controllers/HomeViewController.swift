//
//  ViewController.swift
//  weatherReport
//
//  Created by primeIT on 22/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ForecastProviderDelegate {
    //MARK: Properties
    var mCurrentWeather: Forecast?
    var mForecasts: [Forecast]?
    var mForecastsProvider: ForecastProvider!
    let mRefreshControl = UIRefreshControl()
    var mWeatherRefresh = false{
        didSet{
            endRefresh()
        }
    }
    var mForecastRefresh = false{
        didSet{
            endRefresh()
        }
    }
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mCurrentWeatherView: CurrentWeatherView!
    
    //MARK: Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        refresh()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mForecastsProvider = ServerForecastProvider(withDelegate: self)
        
        mTableView.tableFooterView = UIView()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        mTableView.dataSource = self
        
        mRefreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        mRefreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        
        mTableView.addSubview(mRefreshControl)
    }
    
    //MARK: Private methods
    func endRefresh(){
        mRefreshControl.endRefreshing()
    }
    
    //MARK: Refresh control
    @objc func refresh(){
        mForecastsProvider.getCurrentWeater(city: UserDefaultUtils.getLocation())
        mForecastsProvider.getForecast(city: UserDefaultUtils.getLocation(), forDays: UserDefaultUtils.getForecastDays())
        mWeatherRefresh = true
        mForecastRefresh = true
    }

    //MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mForecasts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ForecastCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ForecastTableViewCell else{
            fatalError("The dequeed cell is of the wrong type")
        }
        guard let forecast = mForecasts?[indexPath.row] else{
            return cell
        }
        cell.mDay = forecast.mDate
        cell.mIcon = forecast.mIcon
        cell.mMinTemp = forecast.mMinTemp
        cell.mMaxTemp = forecast.mMaxTemp
        
        return cell;
    }
    
    //MARK: ForecastProviderDelegate
    func onCurrentWeather(weather: Forecast) {
        DispatchQueue.main.async { [unowned self] in
            self.mCurrentWeather = weather
            
            self.mCurrentWeatherView.mCity = self.mCurrentWeather?.mCity
            self.mCurrentWeatherView.mState = self.mCurrentWeather?.mState
            self.mCurrentWeatherView.mStateImage = self.mCurrentWeather?.mIcon
            self.mCurrentWeatherView.mDate = self.mCurrentWeather?.mDate
            self.mCurrentWeatherView.mTemperature = self.mCurrentWeather!.mCurrentTemp
            self.mCurrentWeatherView.mMinTemp = self.mCurrentWeather!.mMinTemp
            self.mCurrentWeatherView.mMaxTemp = self.mCurrentWeather!.mMaxTemp
            
            self.mWeatherRefresh = false
        }
    }
    
    func onForecast(forecasts: [Forecast]) {
        DispatchQueue.main.async { [unowned self] in
            self.mForecasts = forecasts
            self.mTableView.reloadData()
            
            self.mForecastRefresh = false
        }
    }

}

