//
//  HidingFooterViewController.swift
//  weatherReport
//
//  Created by primeIT on 25/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

///Helper delegate to aid in creating footer views that hide whenever a scrollview scrolls up and reshow them when it scrolls down
class HidingFooterScrollViewDelegate:NSObject, UIScrollViewDelegate{
    //MARK: Properties
    var mInitialScrollY: CGFloat = 0
    var mIsDragging = false
    let mDelegate: HidingFooterDelegate
    
    required init(withDelegate delegate: HidingFooterDelegate){
        mDelegate = delegate
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        mIsDragging = true
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        mIsDragging = false
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let diff = scrollView.contentOffset.y - mInitialScrollY
        print(diff)
        mInitialScrollY = scrollView.contentOffset.y
        if mIsDragging{
            if diff > 5 {
                mDelegate.hideFooter()
            }else if diff < -5{
                mDelegate.showFooter()
            }
        }
    }
}
protocol HidingFooterDelegate{
    func hideFooter()
    func showFooter()
}
