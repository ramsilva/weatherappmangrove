//
//  CurrentWeatherView.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

class CurrentWeatherView: UIView {

    //MARK: properties
    @IBOutlet weak var mCityLabel: UILabel!
    @IBOutlet weak var mStateLabel: UILabel!
    @IBOutlet weak var mStateImageView: UIImageView!
    @IBOutlet weak var mTemperatureLabel: UILabel!
    @IBOutlet weak var mDateLabel: UILabel!
    @IBOutlet weak var mMinTempLabel: UILabel!
    @IBOutlet weak var mMaxTempLabel: UILabel!
    
    var mCity: String!{
        didSet{
            mCityLabel.text = mCity.lowercased().capitalizingFirstLetter()
        }
    }
    var mState: ForecastState!{
        didSet{
            mStateLabel.text = NSLocalizedString(mState.rawValue, comment: "")
        }
    }
    var mStateImage: UIImage!{
        didSet{
            mStateImageView.image = mStateImage
        }
    }
    var mTemperature: Float!{
        didSet{
            mTemperatureLabel.text = TemperatureUtils.getConvertedTemperature(from: mTemperature, withDecimals: 1)
        }
    }
    var mDate: String!{
        didSet{
            mDateLabel.text = mDate
        }
    }
    var mMinTemp: Float!{
        didSet{
            mMinTempLabel.text = TemperatureUtils.getConvertedTemperature(from: mMinTemp)
        }
    }
    var mMaxTemp: Float!{
        didSet{
            mMaxTempLabel.text = TemperatureUtils.getConvertedTemperature(from: mMaxTemp)
        }
    }
}
