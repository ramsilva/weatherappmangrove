//
//  Header2Label.swift
//  weatherReport
//
//  Created by primeIT on 26/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

///Medium header label
class Header2Label: CustomLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setStyle()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setStyle()
    }
    
    override func setStyle(){
        super.setStyle()
        font = UIFont(name: font.fontName, size: 24.0)
    }
}
