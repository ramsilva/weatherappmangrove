//
//  Header1Label.swift
//  weatherReport
//
//  Created by primeIT on 26/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

///Big header label
class Header1Label: CustomLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setStyle()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setStyle()
    }
    
    override func setStyle(){
        super.setStyle()
        font = UIFont(name: font.fontName, size: 36.0)
    }
}
