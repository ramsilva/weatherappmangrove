//
//  CustomLabel.swift
//  weatherReport
//
//  Created by primeIT on 26/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

///Default custom label
class CustomLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setStyle()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setStyle()
    }
    
    func setStyle(){
        font = UIFont(name: "Lato-Regular", size: 16.0)
    }
}
