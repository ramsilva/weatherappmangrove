//
//  ForecastTableViewCell.swift
//  weatherReport
//
//  Created by primeIT on 22/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var mDayLabel: UILabel!
    @IBOutlet weak var mForecastIcon: UIImageView!
    @IBOutlet weak var mMinTempLabel: UILabel!
    @IBOutlet weak var mMaxTempLabel: UILabel!
    
    var mDay: String!{
        didSet{
            mDayLabel.text = mDay.lowercased().capitalizingFirstLetter()
        }
    }
    var mIcon: UIImage!{
        didSet{
            mForecastIcon.image = mIcon
        }
    }
    var mMinTemp: Float!{
        didSet{
            mMinTempLabel.text = TemperatureUtils.getConvertedTemperature(from: mMinTemp)
        }
    }
    var mMaxTemp: Float!{
        didSet{
            mMaxTempLabel.text = TemperatureUtils.getConvertedTemperature(from: mMaxTemp)
        }
    }
}
