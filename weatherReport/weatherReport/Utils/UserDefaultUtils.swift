//
//  UserDefaultUtils.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

///Use this classe's methods to interact with NSUserDefaults in regards to saving settings
class UserDefaultUtils{
    static let LOCATION_KEY = "location key"
    static let UNITS_KEY = "units key"
    static let FORECAST_DAYS_KEY = "forecast days key"
    
    /**
     Saves the current selected location in user defaults.

     - parameter location:
     The location to save
     
     - returns:
     false if the location isn't a recognized one, true otherwise
     */
    static func setLocation(_ location: String?) -> Bool{
        if LocationUtils.checkLocation(location){
            UserDefaults.standard.set(location, forKey: LOCATION_KEY)
            return true
        }
        return false
    }
    
    static func setUnits(_ unit: TemperatureUnit?){
        UserDefaults.standard.set(unit?.rawValue, forKey: UNITS_KEY)
    }
    static func setForecastDays(_ days: Int?){
        UserDefaults.standard.set(days, forKey: FORECAST_DAYS_KEY)
    }
    
    static func getLocation() -> String{
        return UserDefaults.standard.string(forKey: UserDefaultUtils.LOCATION_KEY) ?? "Lisbon"
    }
    
    static func getUnits() -> TemperatureUnit{
        return TemperatureUnit(rawValue: UserDefaults.standard.string(forKey: UserDefaultUtils.UNITS_KEY) ?? TemperatureUnit.Celsius.rawValue)!
    }
    
    static func getForecastDays() -> Int{
        let days = UserDefaults.standard.integer(forKey: UserDefaultUtils.FORECAST_DAYS_KEY)
        
        return days != 0 ? days : 1
    }
}
