//
//  TemperatureUtils.swift
//  weatherReport
//
//  Created by primeIT on 25/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

///Utility class used to convert temperatures between different units as well as formatting into a pretty string
class TemperatureUtils{
    /**
     Returns a pretty string describing a converted temperature
     
     - parameter from: The original temperature in Kelvin.
     - parameter withDecimals: The ammount of decimal points to include in the conversion.
     */
    static func getConvertedTemperature(from temp:Float, withDecimals decimals: Int = 0) -> String{
        var converted: Float = 0.0
        var terminator = ""
        switch UserDefaultUtils.getUnits(){
        case .Celsius:
            terminator = "º"
            converted = temp - 273.15
        case .Fahrenheit:
            terminator = "º"
            converted = (temp * (9/5)) - 459.67
        case .Kelvin:
            converted = temp
        }
        return String(format: "%.\(decimals)f\(terminator)", converted)
    }
}

enum TemperatureUnit: String {
    case Celsius
    case Kelvin
    case Fahrenheit
}
