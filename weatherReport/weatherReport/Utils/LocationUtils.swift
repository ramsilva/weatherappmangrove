//
//  LocationUtils.swift
//  weatherReport
//
//  Created by primeIT on 25/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

///Utility class used to convert between city names and respective api codes
class LocationUtils{
    static private let mLocations = ["lisbon":"2267057", "rotterdam":"2747890"]
    static func checkLocation(_ location: String?) -> Bool{
        return mLocations[location?.lowercased() ?? ""] != nil
    }
    
    /**
     Returns a pretty string with all the available cities concatenated
     */
    static func getAvailableCities() -> String{
        var cities = ""
        for city in mLocations{
            cities += city.key.lowercased().capitalizingFirstLetter() + ", "
        }
        cities = String(cities.prefix(cities.count - 2))
        return cities
    }
    
    static func getCityCode(_ city: String) -> String{
        return mLocations[city.lowercased()] ?? "Lisbon"
    }
}
