//
//  ForecastUtils.swift
//  weatherReport
//
//  Created by primeIT on 26/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation

class ForecastUtil{
    static func getForecasts(fromJson json: [String:Any], forDays days: Int) -> [Forecast]?{
        guard let list = json["list"] as? [[String:Any]] else{
            return nil
        }
        var forecasts = [Forecast]()
        var date = Date()
        let calendar = Calendar.current
        for i in 0..<days{
            guard
                let weather = list[8*i]["weather"] as? [[String:Any]] ,
                let stateSring = weather[0]["main"] as? String else{
                    return nil
            }
            var minTemp = Float(Int.max)
            var maxTemp = Float(Int.min)
            for j in 0..<8{
                let index = (8*i + j) >= list.count ? list.count - 1 : 8*i + j
                guard
                    let main = list[index]["main"] as? [String:Any],
                    let temp = main["temp"] as? Float else{
                        return nil
                }
                if temp < minTemp{
                    minTemp = temp
                }
                if temp > maxTemp{
                    maxTemp = temp
                }
            }
            let dateString = i > 0 ? date.dayOfTheWeek()! : NSLocalizedString("Tomorrow", comment: "")
            let state = ForecastState(rawValue: stateSring)
            forecasts.append(Forecast(date: dateString, minTemp: minTemp, maxTemp: maxTemp, state: state ?? .None))
            
            date = calendar.date(byAdding: .day, value: 1, to: date)!
        }
        
        return forecasts
    }
}
