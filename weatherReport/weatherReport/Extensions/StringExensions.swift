//
//  StringExensions.swift
//  weatherReport
//
//  Created by primeIT on 25/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation
extension String{
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
