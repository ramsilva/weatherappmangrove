//
//  NSDateExtensions.swift
//  weatherReport
//
//  Created by primeIT on 23/12/17.
//  Copyright © 2017 mangrove. All rights reserved.
//

import Foundation
extension Date{
    func dayOfTheWeek() -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(for: self)
    }
}
